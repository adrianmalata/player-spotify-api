# Player Spotify API

***

### Type

Web-based music management tool

### Description

Player Spotify API is an application designed for seamless integration with Spotify,
enabling users to effortlessly search for albums,
select playback devices, and start music playback directly from the app.

Leveraging Spotify’s API and secure OAuth 2.0 authentication,
this application provides an intuitive interface for managing music.

**ATTENTION!**\
You need a premium account on Spotify to actually listen to music with this app.

#### Key Features:

* **Search for Albums**: Quickly find your favorite albums using Spotify’s advanced search capabilities.
* **Device Selection**: Choose from available playback devices registered to your Spotify account.
* **Music Playback**: Start playing selected albums on your chosen device with ease.

### Guide

1. Spotify Developer Setup:
   * Register your application on the Spotify Developer Dashboard to get Client ID and Client Secret.
   * Add your Redirect URI (e.g., http://localhost:8080/callback) in the Spotify Developer Dashboard.

2. Configuration:
   * Update the Client ID, Client Secret, and Redirect URI in your application code.
   * Ensure proper network settings for socket connections (e.g., ports).
   
3. Running the Application:
   * Start your application. 
   * Authenticate with Spotify and authorize the application. 
   * Search for albums, select a playback device, and start enjoying your music.

### Libraries

* Java 21
* Spotify Web API
* OAuth 2.0
* Okhttp 4.12
* Ratpack 1.10
* SLF4J 1.7

***