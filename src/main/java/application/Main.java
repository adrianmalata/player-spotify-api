package application;

import handlers.Handlers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ratpack.core.server.RatpackServer;

import java.nio.file.Path;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws Exception {
        Handlers handlers = new Handlers();
        RatpackServer.start(server -> server
            .serverConfig(config -> config
                    .baseDir(Path.of("/Users/adrianmalata/Desktop/Za Rączkę/Player-Spotify-API/src/main/resources"))
                    .port(8080))
            .handlers(chain -> chain
                    .get("search", handlers::handleSearchAlbumForm)
                    .post("login", handlers::handleLogin)
                    .get("callback", handlers::handleCallback)
                    .get("token", handlers::handleGetTokenEndpoint)
                    .get("player", handlers::handlePlayerSiteRun)
                    .get("scripts/spotifyPlayer.js", handlers::handlePlayerScriptRun)
                    .all(context -> context.render("Nieobsługiwane żądanie.")))
        );
        LOGGER.info("Server started on port 8080");
    }
}
