package handlers;

import application.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ratpack.core.form.Form;
import ratpack.core.handling.Context;
import services.SpotifySearchService;
import services.SpotifyAuthService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class Handlers {
    private static final Logger LOGGER = LoggerFactory.getLogger(Handlers.class);
    public static final String ACCESS_TOKEN_ERROR = "Error: Could not get access token";
    public static final String MISSING_CODE_PARAMETER_ERROR = "Error: Missing code parameter";
    private static String albumName;

    public void handleSearchAlbumForm(Context context) {
        context.getResponse().contentType("text/html");
        context.render(
                "<form method='post' action='login'>" +
                        "<label for='albumName'>Enter album name:</label><br>" +
                        "<input type='text' id='albumName' name='albumName'><br>" +
                        "<input type='submit' value='Search & Play'>" +
                        "</form>");
        LOGGER.info("Searching for album name");
    }

    public void handleLogin(Context context) {
        context.parse(Form.class).then(form -> {
            getAlbumName(form);
            redirectToAuthUrl(context);
        });
    }

    public void handleCallback(Context context) throws IOException {
        String authCode = context.getRequest().getQueryParams().get("code");
        LOGGER.info("Got authCode");

        if (!authCode.isEmpty()) {
            authenticateAndRedirectUser(context, authCode);
        } else {
            handleError(context, 400, MISSING_CODE_PARAMETER_ERROR);
        }
    }

    public void handleGetTokenEndpoint(Context context) {
        String token = SpotifyAuthService.getAccessToken();
        context.render(token);
    }

    public void handlePlayerSiteRun(Context context) throws IOException {
        String html = readResourceAsStream("/static/Player.html");
        if (html != null) {
            context.getResponse().contentType("text/html");
            context.render(html);
        }
    }

    public void handlePlayerScriptRun(Context context) throws IOException {
        String spotifyPlayerScript = readResourceAsStream("/scripts/spotifyPlayer.js");
        if (spotifyPlayerScript != null) {
            context.render(spotifyPlayerScript);
        }
    }

    public void handleError(Context context, int statusCode, String errorMessage) {
        context.getResponse().status(statusCode);
        context.render(errorMessage);
    }

    private void redirectToAuthUrl(Context context) {
        context.redirect(SpotifyAuthService.getAuthUrl());
        LOGGER.info("Got login redirection");
    }

    private void getAlbumName(Form form) {
        albumName = form.get("albumName");
        LOGGER.info("Got album name: {}", albumName);
    }

    private void authenticateAndRedirectUser(Context context, String authCode) throws IOException {
        SpotifyAuthService.fetchAccessToken(authCode);
        LOGGER.info("Fetched access token");

        if (!SpotifyAuthService.getAccessToken().isEmpty()) {
            redirectUserToPlayer(context);
        } else {
            handleError(context, 500,ACCESS_TOKEN_ERROR);
        }
    }

    private void redirectUserToPlayer(Context context) {
        try {
            String albumId = SpotifySearchService.getAlbumId(albumName);
            context.redirect("/player?albumId=" + albumId);
        } catch (IOException e) {
            handleError(context, 500, "An error occurred while executing the searchAlbum method: " + e.getMessage());
        }
    }

    private String readResourceAsStream(String resourcePath) throws IOException {
        try (InputStream inputStream = Main.class.getResourceAsStream(resourcePath)) {
            if (inputStream != null) {
                return new BufferedReader(new InputStreamReader(inputStream))
                        .lines()
                        .collect(Collectors.joining("\n"));
            }
        }
        return null;
    }
}