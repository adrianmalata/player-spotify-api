package services;

import lombok.Getter;
import okhttp3.*;
import org.json.JSONObject;
import utilities.Config;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class SpotifyAuthService {

    private static final OkHttpClient client = new OkHttpClient();
    @Getter
    private static String accessToken;

    public static String getAuthUrl() {
        return "https://accounts.spotify.com/authorize?client_id=" + Config.CLIENT_ID +
                "&response_type=code&redirect_uri=" + URLEncoder.encode(Config.REDIRECT_URI, StandardCharsets.UTF_8) +
                "&scope=user-read-private%20user-read-email%20streaming%20user-read-playback-state";
    }

    public static void fetchAccessToken(String authCode) throws IOException {
        Request request = createAccessTokenRequest(authCode);
        processAccessTokenRequest(request);
    }

    private static Request createAccessTokenRequest(String authCode) {
        RequestBody formBody = new FormBody.Builder()
                .add("grant_type", "authorization_code")
                .add("code", authCode)
                .add("redirect_uri", Config.REDIRECT_URI)
                .add("client_id", Config.CLIENT_ID)
                .add("client_secret", Config.CLIENT_SECRET)
                .build();

        return new Request.Builder()
                .url(Config.TOKEN_URL)
                .post(formBody)
                .build();
    }

    private static void processAccessTokenRequest(Request request) throws IOException {
        try (Response response = client.newCall(request).execute()) {
            if (response.isSuccessful()) {
                assert response.body() != null;
                String jsonData = response.body().string();
                JSONObject jsonObject = new JSONObject(jsonData);
                accessToken = jsonObject.getString("access_token");
            }
        }
    }
}