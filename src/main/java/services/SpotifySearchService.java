package services;

import okhttp3.*;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class SpotifySearchService {
    private static final OkHttpClient CLIENT = new OkHttpClient();

    public static String getAlbumId(String albumName) throws IOException {
        String jsonData = executeRequest(albumName);
        return parseAlbumId(jsonData);
    }

    private static String executeRequest(String albumName) throws IOException {
        Request request = new Request.Builder()
                .url("https://api.spotify.com/v1/search?q=" + URLEncoder.encode(albumName, StandardCharsets.UTF_8) +"&type=album&limit=1")
                .addHeader("Authorization", "Bearer " + SpotifyAuthService.getAccessToken())
                .build();

        try (Response response = CLIENT.newCall(request).execute()) {
            assert response.body() != null;
            return response.body().string();
        }
    }

    private static String parseAlbumId(String jsonData) {
        JSONObject jsonObject = new JSONObject(jsonData);
        return jsonObject
                .getJSONObject("albums")
                .getJSONArray("items")
                .getJSONObject(0)
                .getString("id");
    }
}