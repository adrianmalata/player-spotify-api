package utilities;

public class Config {
    public static final String CLIENT_ID = "47c367618fba4dc38d9873c4e661688f";
    public static final String CLIENT_SECRET = "ef78d85473b3493aa67b1eeaf8b89a5f";
    public static final String REDIRECT_URI = "http://localhost:8080/callback";
    public static final String AUTH_URL = "https://accounts.spotify.com/authorize";
    public static final String TOKEN_URL = "https://accounts.spotify.com/api/token";
    public static final String SEARCH_API = "https://api.spotify.com/v1/search";
}
