window.onSpotifyWebPlaybackSDKReady = async () => {
    let tokenResponse = await fetch('/token');
    let token = await tokenResponse.text();
    let urlParams = new URLSearchParams(window.location.search);
    let albumId = urlParams.get('albumId');

    await updateAlbumAndArtistName(albumId, token);
    await updateTrackName(albumId, token);
    connectToPlayer(token, albumId);
};

async function updateAlbumAndArtistName(albumId, token) {
    let fetchResponse = await fetch(`https://api.spotify.com/v1/albums/${albumId}`, {
        headers: {
            'Authorization': `Bearer ${token}`
        },
    });

    let data = await fetchResponse.json();
    document.getElementById('albumName').innerText = data.name;
    document.getElementById('artistName').innerText = data["artists"][0].name;
}

async function updateTrackName(albumId, token) {
    let fetchResponse = await fetch(`https://api.spotify.com/v1/albums/${albumId}/tracks?limit=1`, {
        headers: {
            'Authorization': `Bearer ${token}`
        },
    });

    let data = await fetchResponse.json();
    document.getElementById('trackName').innerText = data["items"][0].name;
}

function connectToPlayer(token, albumId) {
    let player = new Spotify.Player({
        name: 'Web Playback SDK Quick Start Player',
        getOAuthToken: callback => {
            callback(token);
        },
        volume: 0.5
    });

    setPlayerListeners(player, token, albumId);
    document.getElementById('togglePlay').onclick = function () { player.togglePlay(); };
    player.connect();
}

async function setPlayerListeners(player, token, albumId) {
    player.addListener('ready', ({device_id}) => {
        console.log('Ready with Device ID', device_id);
        playMusic(player, device_id, albumId, token);
    });

    player.addListener('not_ready', ({device_id}) => { console.log('Device ID has gone offline', device_id); });
    player.addListener('initialization_error', ({message}) => { console.error(message); });
    player.addListener('authentication_error', ({message}) => { console.error(message); });
    player.addListener('account_error', ({message}) => { console.error(message); });
}

async function playMusic(player, device_id, albumId, token) {
    let response = await fetch(`https://api.spotify.com/v1/me/player/play?device_id=${device_id}`, {
        method: 'PUT',
        body: JSON.stringify({ uris: [`spotify:album:${albumId}`] }),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    });
}